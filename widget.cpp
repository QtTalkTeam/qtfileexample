#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>
#include <QDir>
#include <QImageReader>

#include "widget.h"
#include "ui_widget.h"

//--------------------------------------------------------------------------------------------------------
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}
//--------------------------------------------------------------------------------------------------------
Widget::~Widget()
{
    delete ui;
}

//--------------------------------------------------------------------------------------------------------
bool Widget::readTxtFile(QString fileName){

    QFile file(fileName);

    if(file.open(QFile::ReadOnly | QFile::Text)){

        QTextStream stream(&file);

        QString line;
        do {

            line = stream.readLine();
            ui->textEdit->append( line );

        } while(!line.isNull());

        file.close();

        return(true);
    }

   return(false);
}

//--------------------------------------------------------------------------------------------------------
bool Widget::writeTxtFile(QString fileName){

    QFile file(fileName);

    if(file.open(QFile::WriteOnly | QFile::Text)){

        QTextStream stream(&file);

        stream << ui->textEdit->toPlainText();

        file.close();

        return(true);
    }

    return(false);
}

//--------------------------------------------------------------------------------------------------------
// Button slot
//--------------------------------------------------------------------------------------------------------
void Widget::on_readButton_clicked()
{        
    QString filename =  QFileDialog::getOpenFileName(
          this,
          "Txt File Example",
          QDir::currentPath(),
          "Txt File Example (*.txt);; All files (*.*) ;;");

    if( !filename.isNull() ){

        qDebug() << "selected file path : " << filename;

        if( QFile::exists(filename) ){
            if(readTxtFile(filename)){
                ui->fileNameLabel->setText(filename);
            }
        }

    }else{

        QMessageBox msgBox;
        msgBox.setText("Errore nella lettura, File non aperto!");
        msgBox.exec();
    }
}

//--------------------------------------------------------------------------------------------------------
void Widget::on_writeButton_clicked()
{

    QString filename =  QFileDialog::getSaveFileName(
        this,
        "Save FileName", ui->fileNameLabel->text(),
        "Save Text File (*.txt);;All Files (*)");

    if( !filename.isNull() )
    {

        qDebug() << "selected file path : " << filename;

        writeTxtFile(filename);

    }else{

        QMessageBox msgBox;
        msgBox.setText("Errore nel salvataggio, File non aperto!");
        msgBox.exec();
    }
}

//--------------------------------------------------------------------------------------------------------
void Widget::readBinFile(QString filename){


    QFileInfo fInfo(filename);
    if(fInfo.exists()){

        qDebug() << "FileSize :" << fInfo.size();

        QFile file(filename);

        if(file.open(QIODevice::ReadOnly)){

            QDataStream in(&file);

            //Int

            qint32 qi;
            in >> qi;
            ui->qIntLineEdit->setText(QString("%1").arg(qi));

            // ByteArray

            QByteArray  b;
            in >> b;
            ui->qByteArrayLineEdit->setText(b.toHex());

            //String

            QString s;
            in >> s;
            ui->qStringLineEdit->setText(s);

            //Image

            in >> s;
            ui->qImageLineEdit->setText(s);

            QPixmap img;
            in >> img;

            ui->imageLabel->setPixmap(img);

            file.close();

        }
    }
}
//--------------------------------------------------------------------------------------------------------
void Widget::writeBinFile(QString filename){

    // pippo.bin

    QFileInfo fInfo(filename);

    //
    // Test estensione del File
    //
    QString ext = fInfo.suffix();

    qDebug() << ext ;

    if(ext != QString::null){
        //
        // controlliamo se è .bin
        //
        if(ext.toLower() != "bin"){

            QString filename_noext = fInfo.baseName();

            qDebug() << filename_noext;

            filename = filename_noext.append(".bin");
        }

    }else{
        //
        // Non ha estensione -> la aggiungiamo
        //
        filename.append(".bin");
    }

    qDebug() << "save on: " << filename;

    //
    // Salvataggio del File
    //
    QFile file(filename);

    if(file.open(QIODevice::WriteOnly)){

        QDataStream out(&file);

        //Int

        out << qint32 (ui->qIntLineEdit->text().toInt());

        //Byte Array

        QByteArray  b;
        QStringList sl = ui->qByteArrayLineEdit->text().split(',');
        foreach (QString s, sl) {
           b.append(s);
        }

        qDebug() << b.toHex();

        out << b;

        // String

        out << ui->qStringLineEdit->text();

        // Image

        out << ui->qImageLineEdit->text();

        QPixmap img(ui->qImageLineEdit->text());

        out << img;

        // Close File

        file.flush();

        file.close();
    }
}

//--------------------------------------------------------------------------------------------------------
void Widget::on_readBinFilePushButton_clicked()
{

    QString filename =  QFileDialog::getOpenFileName(
          this,
          "Bin File Example",
          QDir::currentPath(),
          "Bin File Example (*.bin);; All files (*.*) ;;");

    if( !filename.isNull() )
    {
      qDebug() << "selected file path : " << filename.toUtf8();

      readBinFile(filename);
    }


}
//--------------------------------------------------------------------------------------------------------
void Widget::on_clearBinFilePushButton_clicked()
{
    ui->qIntLineEdit->clear();
    ui->qByteArrayLineEdit->clear();
    ui->qStringLineEdit->clear();
    ui->qImageLineEdit->clear();
    ui->imageLabel->setPixmap(QPixmap());
}
//--------------------------------------------------------------------------------------------------------

void Widget::on_saveBinFilePushButton_clicked()
{
    QString filename =  QFileDialog::getSaveFileName(
          this,
          "Bin File Example",
          QDir::currentPath(),
          "Bin File Example (*.bin);; All files (*.*) ;;");

    if( !filename.isNull() )
    {
      qDebug() << "selected file path : " << filename.toUtf8();

      writeBinFile(filename);
    }

}
//--------------------------------------------------------------------------------------------------------
void Widget::on_cleatTextButton_clicked()
{
    ui->textEdit->clear();
}

//--------------------------------------------------------------------------------------------------------
qlonglong Widget::imageSpace(QString dirpath){

    QDir dir(dirpath);
    qlonglong size = 0;

    QStringList filters;

    filters += "*.jpg";
    filters += "*.png";

    ui->imageSizeTextEdit->append(QString("Start from: -> %1 ").arg(dirpath));

    foreach (QString file, dir.entryList(filters, QDir::Files)) {

        size += QFileInfo(dir,file).size();

        ui->imageSizeTextEdit->append(QString("File: %1 -> Size: %2 bytes").arg(file).arg(QFileInfo(dir,file).size()));
    }

    foreach(QString subdir, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)){

        ui->imageSizeTextEdit->append(QString("Sub Dir -> %1 ").arg(dirpath + QDir::separator() + subdir));

        size += imageSpace(dirpath + QDir::separator() + subdir);
    }

    return size;
}

//--------------------------------------------------------------------------------------------------------
void Widget::on_selDirPushButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "/home",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);

    if(! dir.isNull() ){

        ui->imageSizeTextEdit->clear();

        qlonglong size = imageSpace(dir);

        ui->imageSizeTextEdit->append(QString("-------------------------"));
        ui->imageSizeTextEdit->append(QString("Total size: %1").arg(size));
    }
}
