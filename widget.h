#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_readButton_clicked();

    void on_writeButton_clicked();

    void on_readBinFilePushButton_clicked();

    void on_clearBinFilePushButton_clicked();

    void on_saveBinFilePushButton_clicked();

    void on_cleatTextButton_clicked();

    void on_selDirPushButton_clicked();

private:
    Ui::Widget *ui;

    bool readTxtFile(QString fileName);
    bool writeTxtFile(QString fileName);

    void readBinFile(QString filename);
    void writeBinFile(QString filename);

    qlonglong imageSpace(QString dirpath);
};

#endif // WIDGET_H
